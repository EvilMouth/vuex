import Vue from 'vue'

const emptyMessage = {
  isRead: true
}

export default {
  receiveAll (state, messages) {
    let latestMessage
    messages.forEach(message => {
      // create new thread if the thread doesn't exist
      if (!state.threads[message.threadID]) {
        createThread(state, message.threadID, message.threadName)
      }
      // mark the latest message
      if (!latestMessage || message.timestamp > latestMessage.timestamp) {
        latestMessage = message
      }
      // add message
      addMessage(state, message)
    })
    // set initial thread to the one with the latest message
    setCurrentThread(state, latestMessage.threadID)
  },

  receiveMessage (state, message) {
    addMessage(state, message)
  },

  deleteMessage (state, message) {
    removeMessage(state, message)
  },

  switchThread (state, id) {
    setCurrentThread(state, id)
  }
}

function createThread (state, id, name) {
  Vue.set(state.threads, id, {
    id,
    name,
    messages: [],
    lastMessage: null
  })
}

function addMessage (state, message) {
  // add a `isRead` field before adding the message
  message.isRead = message.threadID === state.currentThreadID
  // add it to the thread it belongs to
  const thread = state.threads[message.threadID]
  if (!thread.messages.some(id => id === message.id)) {
    thread.messages.push(message.id)
    thread.lastMessage = message
  }
  // add it to the messages map
  Vue.set(state.messages, message.id, message)
}

function removeMessage (state, message) {
  // delete it from the thread it belongs to
  const thread = state.threads[message.threadID]
  thread.messages.forEach((id, index) => {
    if (id === message.id) {
      thread.messages.splice(index, 1)
      const length = thread.messages.length
      if (length) {
        const lastMessageId = thread.messages[length - 1]
        const lastMessage = state.messages[lastMessageId]
        lastMessage.isRead = true
        thread.lastMessage = lastMessage
      } else {
        thread.lastMessage = emptyMessage
      }
      // delete it from the messages map
      Vue.delete(state.messages, message.id)
      return
    }
  })
}

function setCurrentThread (state, id) {
  state.currentThreadID = id
  if (!state.threads[id]) {
    debugger
  }
  // mark thread as read
  state.threads[id].lastMessage.isRead = true
}
